/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.claveunica.controller;

import com.prodigio.claveunica.method.GetAccessToken;
import com.prodigio.claveunica.method.GetJbossToken;
import com.prodigio.claveunica.method.GetUserData;
import com.prodigio.claveunica.method.UserValidator;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;
import org.xml.sax.SAXException;

/**
 *
 * @author Prodigio
 */
@Controller
public class CallbackController {

    @RequestMapping("/callback")
    public String callback(@RequestParam(name = "state", required = false, defaultValue = "") String state,
            @RequestParam(name = "code", required = false, defaultValue = "") String code, Model model, HttpServletRequest request) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        String redirectUrl = "";
        
        //obtener token de acceso directo en CU
        GetAccessToken token = new GetAccessToken();
        
        //obtener token de acceso a traves de JBoss
        GetJbossToken token2 = new GetJbossToken();
        
        //obtener datos de usuario en CU
        GetUserData data = new GetUserData();
        JSONObject json = new JSONObject(data.UserData(token.AccessToken(state, code)));
//        JSONObject json = new JSONObject(data.UserData(token2.JbossToken(state, code)));

        model.addAttribute("rut", json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV"));

//        return "results";

        //Obtener validacion de la existencia del usuario en la API de aleph
        UserValidator uv = new UserValidator();
//        JSONObject validacion = new JSONObject(uv.validator(json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV")));
////
        String password = "";

        switch (json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV")) {
            case "444444444":
                password = json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV");
                break;
            case "888888888":
                password = json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV");
                break;
            case "999999999":
                password = json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV");
                break;
            default:
                password = "";
                break;
        }
        redirectUrl = request.getScheme() + "://www.bncatalogo.gob.cl/F?func=login-session&login_source=&bor_id=" + json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV") + "&bor_verification=" + password + "&bor_library=BNC50";
//        return "redirect:" + redirectUrl;

        model.addAttribute("itsok",uv.validator(json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV")));
//        return "results";
        if ("ok".equals(uv.validator(json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV")))) {
//            redirectUrl = request.getScheme() + "://www.bncatalogo.cl:1891/rest-dlf/patron/" + json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV") + "/patronInformation/address";
            redirectUrl = request.getScheme() + "://www.bncatalogo.gob.cl/F/?func=login-session&login_source=&bor_id=" + json.getJSONObject("RolUnico").getInt("numero") + json.getJSONObject("RolUnico").getString("DV") + "&bor_verification="+password+"&bor_library=BNC50";
            return "redirect:" + redirectUrl;
//            return "results";

        } else {
            /*redirectUrl = request.getScheme() + "://www.bncatalogo.gob.cl/F/BNJ5CEIPXSGMIU69S8SS8I3S5LALXMG6DPD379SUE3HNF4VEH3-21533?func=bor-new-0";
            return "redirect:" + redirectUrl;  */
            return "results";
        }
    }
}
