package com.prodigio.claveunica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClaveunicaApplication {

    public static void main(String[] args) {
        
        SpringApplication.run(ClaveunicaApplication.class, args);
    }

}
