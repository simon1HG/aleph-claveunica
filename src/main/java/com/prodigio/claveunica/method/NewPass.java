/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.claveunica.method;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Prodigio
 */
public class NewPass {

    public String Password(String rut) {
        String url = "http://www.bncatalogo.cl:1891/rest-dlf/patron/" + rut + "/patronInformation/password";
        String pass = "", newpass = "";
        try {
            System.getProperties().put("proxySet", "true");
            System.getProperties().put("socksProxyHost", "127.0.0.1");
            System.getProperties().put("socksProxyPort", "8081");
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/xml");
            con.setRequestProperty("Accept", "application/xml");
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
            System.out.println("Send 'HTTP POST' request to : " + url);

            String urlParameters = "<?xml version = \"1.0\" encoding = \"UTF-8\"?>\n"
                    + "<reply-text>ok</reply-text>\n"
                    + "<reply-code>0000</reply-code>\n"
                    + "<get-pat-pswd>\n"
                    + "    <password_parameters>\n"
                    + "        <old-password>" + pass + "</old-password>\n"
                    + "        <new-password>" + newpass + "</new-password>\n"
                    + "    </password_parameters>\n"
                    + "</get-pat-pswd>";
            con.setDoOutput(true);
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.writeBytes(urlParameters);
                wr.flush();
            } catch (IOException ex) {
                Logger.getLogger(GetAccessToken.class.getName()).log(Level.SEVERE, null, ex);
            }

            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader input2Reader = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = input2Reader.readLine()) != null) {
                    response.append(inputLine);
                }
                input2Reader.close();

                System.out.println(response.toString());

                return "pass";
            }
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException Excepción en la clase Consume: " + ex);
        } catch (ProtocolException ex) {
            System.out.println("ProtocolException Excepción en la clase Consume: " + ex);
        } catch (IOException ex) {
            System.out.println("IOException Excepción en la clase Consume: " + ex);
        }
        return null;
    }
}
