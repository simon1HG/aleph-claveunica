package com.prodigio.claveunica.method;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;


/**
 *
 * @author Prodigio
 */
public class UserValidator {
    public String validator(String rut){
        
        String url = "http://www.bncatalogo.cl:1891/rest-dlf/patron/"+rut+"/patronInformation/address";
        try {
            System.getProperties().put( "proxySet", "true" );
            System.getProperties().put( "socksProxyHost", "127.0.0.1" );
            System.getProperties().put( "socksProxyPort", "8081" );
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
            System.out.println("Send 'HTTP POST' request to : " + url);

            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader input2Reader = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = input2Reader.readLine()) != null) {
                    response.append(inputLine);
                }
                input2Reader.close();
                

                //System.out.println(response.toString());
                
                JSONObject validacion = new JSONObject(response.toString());
                String resp = validacion.getJSONObject("get-pat-adrs").getString("reply-text");
                return resp;
                
            }
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException Excepción en la clase Consume: " + ex);
        } catch (ProtocolException ex) {
            System.out.println("ProtocolException Excepción en la clase Consume: " + ex);
        } catch (IOException ex) {
            System.out.println("IOException Excepción en la clase Consume: " + ex);
        }
        return null;
    }
}
