/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.claveunica.method;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;

/**
 *
 * @author Prodigio
 */
public class GetUserData {

    public String UserData(String token) {
        String url = "https://www.claveunica.gob.cl/openid/userinfo/";
        try {
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
            con.setRequestProperty("authorization", "Bearer " + token);
            System.out.println("Send 'HTTP POST' request to : " + url);

            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                BufferedReader input2Reader = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = input2Reader.readLine()) != null) {
                    response.append(inputLine);
                }
                input2Reader.close();
                System.out.println(token);
                return response.toString();
            }
        }catch (MalformedURLException ex) {
            System.out.println("MalformedURLException Excepción en la clase Consume: " + ex);
        } catch (ProtocolException ex) {
            System.out.println("ProtocolException Excepción en la clase Consume: " + ex);
        } catch (IOException ex) {
            System.out.println("IOException Excepción en la clase Consume: " + ex);
        }
        return null;
    }

}
