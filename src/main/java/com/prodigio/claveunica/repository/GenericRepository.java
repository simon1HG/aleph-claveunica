/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prodigio.claveunica.repository;

/**
 *
 * @author Prodigio
 */

public interface GenericRepository<T> {
    T create(T t);
    void delete (T t);
    T find (T t);
    T update (T t);
    
    Iterable<T> findAll();
}
