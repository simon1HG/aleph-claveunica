/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package com.prodigio.claveunica;



import com.prodigio.claveunica.method.GetAccessToken;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPathExpressionException;

/**
 *
 * @author Prodigio
 */
public class Prueba {

    public static void main(String[] args) throws XPathExpressionException {
        String state = "9ed39e2ea931586b6a985a6942ef573e";
        String code = "998afbdd1e434ae7af5de7d243090732";
        //String url = "http://www.bncatalogo.cl:1891/rest-dlf/patron/444444444";
        //String url = "http://www.bncatalogo.cl/X?op=bor-info&bor_id=444444444&library=bnc50&loans=N&cash=N&hold=N";
        String token = "59a6a31842ed4bf483adbd8f98e43b49";
//        String url = "http://www.bncatalogo.cl:1891/rest-dlf/patron/444444444/patronInformation/address";
        String url = "http://www.bncatalogo.cl:1891/rest-dlf/patron/444444444/patronInformation/password";
        //String url = "http://www.bncatalogo.cl:1891/AlephWebServices/services/Login?wsdl";
//        String url = "http://www.bncatalogo.cl:1891/rest-dlf/patron";

//            String urlParameters = "<get-new-patron>\n"
//                    + "<reply-text>ok</reply-text>\n"
//                    + "<reply-code>0000</reply-code>\n"
//                    + "<new-patron>\n"
//                    + "<patron-name></patron-name>\n"
//                    + "<internal-id/>\n"
//                    + "<local-sub-library/>\n"
//                    + "<internal-id-verification/>\n"
//                    + "<con-lng usage=\"Mandatory\"/>\n"
//                    + "<address usage=\"Optional\" max_len=\"200\"/>\n"
//                    + "<address-2 usage=\"Optional\" max_len=\"200\"/>\n"
//                    + "<address-3 usage=\"Optional\" max_len=\"200\"/>\n"
//                    + "<address-4 usage=\"Optional\" max_len=\"200\"/>\n"
//                    + "<zip usage=\"Optional\" max_len=\"9\"/>\n"
//                    + "<email-address usage=\"Optional\" max_len=\"60\"/>\n"
//                    + "<telephone-1 usage=\"Mandatory\" max_len=\"30\"/>\n"
//                    + "<telephone-2 usage=\"Optional\" max_len=\"30\"/>\n"
//                    + "<telephone-3 usage=\"Optional\" max_len=\"30\"/>\n"
//                    + "<telephone-4 usage=\"Optional\" max_len=\"30\"/>\n"
//                    + "<sms-number usage=\"Optional\" max_len=\"30\"/>\n"
//                    + "<want-sms usage=\"Optional\" max_len=\"1\">\n"
//                    + "<want-sms-list Code=\"Y\"/>\n"
//                    + "<want-sms-list Code=\"N\"/>\n"
//                    + "</want-sms>\n"
//                    + "<address-type usage=\"Optional\" max_len=\"2\"/>\n"
//                    + "<address-valid-from usage=\"Optional\" max_len=\"8\">20191120</address-valid-from>\n"
//                    + "<address-valid-to usage=\"Optional\" max_len=\"8\">20200520</address-valid-to>\n"
//                    + "<gender usage=\"Optional\" max_len=\"1\"/>\n"
//                    + "<birthplace usage=\"Optional\" max_len=\"30\"/>\n"
//                    + "<birth-date usage=\"Optional\" max_len=\"8\"/>\n"
//                    + "<export-consent usage=\"Optional\" max_len=\"1\">\n"
//                    + "<export-consent-list Code=\"Y\"/>\n"
//                    + "<export-consent-list Code=\"N\"/>\n"
//                    + "</export-consent>\n"
//                    + "<mail-attachment usage=\"Optional\" max_len=\"1\"/>\n"
//                    + "<forgot-password-question usage=\"Optional\" max_len=\"2\"/>\n"
//                    + "<forgot-password-answer usage=\"Optional\" max_len=\"16\"/>\n"
//                    + "<field-1 usage=\"Optional\" max_len=\"200\"/>\n"
//                    + "<field-2 usage=\"Optional\" max_len=\"200\"/>\n"
//                    + "<field-3 usage=\"Optional\" max_len=\"200\"/>\n"
//                    + "<note-1 usage=\"Optional\" max_len=\"200\"/>\n"
//                    + "<note-2 usage=\"Optional\" max_len=\"200\"/>\n"
//                    + "<home-library usage=\"Optional\" max_len=\"5\"/>\n"
//                    + "<additional-id usage=\"Optional\" max_len=\"255\"/>\n"
//                    + "<additional-id-verification usage=\"Optional\" max_len=\"20\"/>\n"
//                    + "</new-patron>\n"
//                    + "</get-new-patron>";
//            con.setDoOutput(true);
//            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
//                wr.writeBytes(urlParameters);
//                wr.flush();
//            } catch (IOException ex) {
//                Logger.getLogger(GetAccessToken.class.getName()).log(Level.SEVERE, null, ex);
//            }
        String pass = "444444444", newpass = "Pr0d1g10";
        try {
            System.getProperties().put("proxySet", "true");
            System.getProperties().put("socksProxyHost", "127.0.0.1");
            System.getProperties().put("socksProxyPort", "8081");
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/xml");
            con.setRequestProperty("Accept", "application/xml");
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
            System.out.println("Send 'HTTP POST' request to : " + url);

            String urlParameters = "<?xml version = '1.0' encoding = 'UTF-8'?>\n"
                    + "<get-pat-pswd>\n"
                    + "<password_parameters>\n"
                    + "<old-password>"+pass+"</old-password>\n"
                    + "<new-password>"+newpass+"</new-password>\n"
                    + "</password_parameters>"
                    + "</get-pat-pswd>";
            con.setDoOutput(true);
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.writeBytes(urlParameters);
                wr.flush();
            } catch (IOException ex) {
                Logger.getLogger(GetAccessToken.class.getName()).log(Level.SEVERE, null, ex);
            }

            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader input2Reader = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = input2Reader.readLine()) != null) {
                    response.append(inputLine);
                }
                input2Reader.close();

                System.out.println(response.toString());

            }
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException Excepción en la clase Consume: " + ex);
        } catch (ProtocolException ex) {
            System.out.println("ProtocolException Excepción en la clase Consume: " + ex);
        } catch (IOException ex) {
            System.out.println("IOException Excepción en la clase Consume: " + ex);
        }
    }

}
